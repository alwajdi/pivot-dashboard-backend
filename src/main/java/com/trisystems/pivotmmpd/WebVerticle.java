package com.trisystems.pivotmmpd;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServer;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.CorsHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;


@Slf4j
@Component
public class WebVerticle extends AbstractVerticle {

    @Override
    public void start(Promise<Void> startPromise) throws Exception {
        log.info("starting Web Verticle!");

        Router router = Router.router(vertx);
        router.route().handler(BodyHandler.create());
        router.route().handler(CorsHandler.create("*")
                .allowedMethod(HttpMethod.GET)
                .allowedMethod(HttpMethod.OPTIONS)
                .allowedHeader("Access-Control-Allow-Origin")
                .allowedHeader("Content-Type")
                .allowedHeader("Access-Control-Allow-Headers"));

        router.route().path("/pivotmmpd/:location")
                .method(HttpMethod.GET)
                .handler(routingContext -> {
                    String location = routingContext.request().getParam("location");
                    log.info("path invoked: {}", location);
                    vertx.eventBus().send("webClient", new JsonObject().put("location", location), res->{
                        if(res.succeeded()){
                            JsonObject result = (JsonObject) res.result().body();
                            routingContext.response().putHeader("content-type", "application/json").end(result.encodePrettily());
                        }
                    });
                });

        HttpServer httpServer = vertx.createHttpServer();
        httpServer.requestHandler(router);
        httpServer.listen(8081, res -> {
            if (res.succeeded()) {
                log.info("WebVerticle deployed successfully");
                startPromise.complete();
            } else {
                log.error("WebVerticle failed to deploy!");
                startPromise.fail(res.cause());
            }
        });
    }
}
