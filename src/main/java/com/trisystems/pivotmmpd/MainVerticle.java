package com.trisystems.pivotmmpd;

import io.vertx.config.ConfigRetriever;
import io.vertx.config.ConfigRetrieverOptions;
import io.vertx.config.ConfigStoreOptions;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.HttpRequest;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.client.WebClientOptions;
import io.vertx.mysqlclient.MySQLConnectOptions;
import io.vertx.mysqlclient.MySQLPool;
import io.vertx.sqlclient.PoolOptions;
import io.vertx.sqlclient.RowSet;
import io.vertx.sqlclient.Tuple;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Slf4j
@Component
public class MainVerticle extends AbstractVerticle {

    private WebClient webClient;
    private JsonObject config;
    private MySQLPool dbClient;

    private Map<String, String> tagWebIdMap = new HashMap<>();

    @Override
    public void start(Promise<Void> startPromise) throws Exception {
        log.info("MainVerticle started!");

        ConfigStoreOptions configStoreOptions = new ConfigStoreOptions()
                .setType("file")
                .setConfig(new JsonObject().put("path", "conf/config.json"));

        ConfigRetriever configRetriever = ConfigRetriever.create(vertx, new ConfigRetrieverOptions()
                .addStore(configStoreOptions));

        configRetriever.getConfig(json -> {
            if (json.succeeded()) {
                config = json.result();
                log.info("config retrieved: {}", config);
                initializeWebClient();
                initializeDb();
                startPromise.complete();
            } else {
                startPromise.fail(json.cause());
            }
        });

        vertx.eventBus().consumer("webClient", this::onMessage);
    }

    private void initializeWebClient() {
        WebClientOptions clientOptions = new WebClientOptions()
                .setSsl(true)
                .setTrustAll(true)
                .setVerifyHost(false)
                .setKeepAlive(true)
                .setTcpKeepAlive(true);

        webClient = WebClient.create(vertx, clientOptions);
    }

    private void initializeDb() {
        log.info("initializing DB");
        MySQLConnectOptions mySQLConnectOptions = new MySQLConnectOptions()
                .setPort(config.getInteger("dbport"))
                .setHost(config.getString("dbhost"))
                .setUser(config.getString("dbusername"))
                .setPassword(config.getString("dbpassword"))
                .setDatabase(config.getString("dbschema"));

        PoolOptions poolOptions = new PoolOptions().setMaxSize(5);

        dbClient = MySQLPool.pool(vertx, mySQLConnectOptions, poolOptions);
        populateMap();
    }

    private void populateMap() {
        log.info("populating map");
        dbClient.query("SELECT tag from tags", ar -> {
            if (ar.succeeded()) {
                RowSet result = ar.result();
                result.forEach(data -> {
                    String tag = data.getString("tag");
                    if (!(tag.equals("tbd") || tag.contains(","))) {
                        log.info("tag: {}", tag);
                        if (tag.contains(" "))
                            tag = tag.replace(" ", "%20"); //escape the space character to comply with web requirement
                        tagWebIdMap.put(tag, "");
                    }
                });
            } else {
                log.error("failed:", ar.cause());
            }

            //insert the corresponding WebId value to the map.
            tagWebIdMap.forEach((key, value) -> {
                HttpRequest<Buffer> clientRequest = webClient.get(443, config.getString("piwebhost"),
                        "/piwebapi/search/query?q=name:" + key)
                        .basicAuthentication(config.getString("username"), config.getString("password"));

                clientRequest.send(ar2 -> {
                    if (ar2.succeeded()) {
                        HttpResponse<Buffer> res = ar2.result();
                        log.info(res.bodyAsString());
                        JsonObject result = res.bodyAsJsonObject();
                        JsonObject object = result.getJsonArray("Items").getJsonObject(0);
                        tagWebIdMap.put(key, object.getString("WebId"));
                        log.info("key: {}, webId: {}", key, object.getString("WebId"));
                    } else {
                        log.error("error occurred while mapping value to key", ar2.cause());
                    }
                });
            });
        });
    }

    private void onMessage(Message<JsonObject> message) {
        String location = message.body().getString("location");
        log.info("webClient to execute query on location {}", location);

        Future<JsonArray> steps = querydbForTagsByLocation(location).compose(this::queryPiServer);
        steps.setHandler(hnd -> {
            if (hnd.succeeded()) {
                log.info("values {}", hnd.result().encodePrettily());
                JsonObject jsonObject = new JsonObject()
                        .put("results", hnd.result());
                message.reply(jsonObject);
            }
        });

    }

    private Future<Map<String, String>> querydbForTagsByLocation(String location) {
        Promise<Map<String, String>> promise = Promise.promise();

        dbClient.preparedQuery("SELECT pitag.tag as tag, ui.ui_field as label from tags pitag left join dashboard_ui ui on pitag.fk_dashboard_ui=ui.id where location=?", Tuple.of(location), ar -> {

            if (ar.succeeded()) {
                RowSet result = ar.result();
                Map<String, String> tagLabelValues = new HashMap<>();
                result.forEach(data -> {
                    String tag = data.getString("tag");
                    if (!(tag.equals("tbd") || tag.contains(","))) {
                        if (tag.contains(" ")) {
                            tag = tag.replace(" ", "%20");
                        }
                        tagLabelValues.put(data.getString("label"),tag);
                    }
                });
                promise.complete(tagLabelValues);

            } else {
                log.error("Error querying db", ar.cause());
                promise.fail(ar.cause());
            }
        });

        return promise.future();
    }

    private Future<JsonArray> queryPiServer(Map<String, String> tags) {
        Promise<JsonArray> tagPromise = Promise.promise();

        JsonArray tagValueArray = new JsonArray();

        tags.forEach((label, tag) -> {
            HttpRequest<Buffer> request = webClient
                    .get(443, config.getString("piwebhost"), "/piwebapi/streams/" + tagWebIdMap.get(tag) + "/value")
                    .basicAuthentication(config.getString("username"), config.getString("password"));


            final String finalTag = tag;
            request.send(ar2 -> {
                if (ar2.succeeded()) {
                    HttpResponse<Buffer> response = ar2.result();
                    JsonObject res = response.bodyAsJsonObject();
                    tagValueArray.add(new JsonObject()
                            .put("label", label)
                            .put("tag", finalTag)
                            .put("value", res.getDouble("Value")));
                } else {
                    log.error("error occurred", ar2.cause());
                    tagPromise.fail(ar2.cause());
                }

                if(tagValueArray.size() == tags.size()){ //when the size matches, considered as completed.
                    tagPromise.complete(tagValueArray);
                }
            });
        });

        return tagPromise.future();
    }
}
