package com.trisystems.pivotmmpd;


import io.vertx.core.Vertx;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;

@Slf4j
@SpringBootApplication
public class PivotMmpdApplication {

    private MainVerticle mainVerticle;
    private WebVerticle webVerticle;

    @Autowired
    public PivotMmpdApplication(MainVerticle mainVerticle,
                                WebVerticle webVerticle){
        this.mainVerticle = mainVerticle;
        this.webVerticle = webVerticle;
    }

    public static void main(String[] args) {
        SpringApplication.run(PivotMmpdApplication.class, args);
    }

    @PostConstruct
    public void deployVerticles(){
        Vertx vertx = Vertx.vertx();
        vertx.deployVerticle(mainVerticle);
        vertx.deployVerticle(webVerticle);
    }

}
